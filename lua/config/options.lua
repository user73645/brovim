local o = vim.opt
-- LINE NUMBERING
o.relativenumber = true
o.number = true

-- FORMATTING
o.shiftwidth = 4
o.numberwidth = 4
o.tabstop = 4
o.expandtab = true

o.wrap = true
o.linebreak = true
o.encoding = 'utf-8'
o.autoindent = true

-- USER INTERFACE
o.hlsearch = false
o.cursorline = false
o.clipboard = 'unnamedplus'
o.scrolloff = 8
o.showmode = false
o.cursorline = true
o.mouse = ''
o.foldmethod = 'syntax'
o.foldnestmax = 10
-- o.nofoldenable = true
o.foldlevel = 2
o.signcolumn = "yes"

-- Backend
o.swapfile = false

-- Enable spellingchecking on LaTeX, markdown, text and for vimwiki
vim.api.nvim_create_autocmd(
    { "BufRead", "BufNewFile" },
    { pattern = { "*.txt", "*.md", "*.tex, *.wiki" }, command = "setlocal spell" }
)

-- Set colorcolumn to 88 on python files
vim.api.nvim_create_autocmd(
    { "BufRead", "BufNewFile" },
    { pattern = {"*.py" }, command = "set colorcolumn=88" }
)

vim.api.nvim_create_autocmd(
    { "BufReadPost", "BufNewFile" },
    { pattern = {"*.wiki" }, command = "set foldmethod=syntax" }
)

vim.api.nvim_create_autocmd(
    { "BufWrite" },
    { pattern = {"*.tex"}, command = "!pdflatex % >/dev/null 2>&1" }
)

-- Format python files with black on write
-- vim.api.nvim_create_autocmd(
--     { "BufWrite" },
--     { pattern = { "*.py" }, command = "Black" }
-- )
-- vim.cmd("let g:vimwiki_list = [{'path': '/home/edmont/.local/share/vimwiki/'}]")

function Synctex()
  -- remove '--silent' for debugging
  vim.fn.system("zathura --synctex-forward " .. vim.fn.line(".") .. ":" .. vim.fn.col(".") .. ":" .. vim.fn.bufname("%") .. " " .. vim.g.syncpdf)
end

vim.api.nvim_set_keymap('n', '<C-Enter>', ':lua Synctex()<CR>', { noremap = true, silent = true })



-- Change wiki based on path 
-- TODO: Make this a switch statement

vim.cmd("let g:vimwiki_list = [{ 'path': '/home/edmont/.local/share/vimwiki/journal' }, {'path': '/home/edmont/.local/share/vimwiki/bth'}, {'path': '/home/edmont/.local/share/vimwiki/pathfinder' }]")
vim.cmd("let g:vimwiki_folding='syntax'")
