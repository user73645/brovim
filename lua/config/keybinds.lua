local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

keymap("n", "E", "ge", opts)

keymap("n", "<C-l>", ":bnext<CR>", opts)
keymap("n", "<C-h>", ":bprev<CR>", opts)

keymap("n", "<leader>fg", ":Telescope live_grep<CR>", opts)
keymap("n", "<leader>fw", ":Telescope grep_string<CR>", opts)
keymap("n", "<leader>ff", ":Telescope find_files<CR>", opts)
keymap("n", "<leader>ne", ":NnnExplorer<CR>", opts)
keymap("n", "<leader>np", ":NnnPicker<CR>", opts)

-- Exit terminal insert mode
keymap("t", "<C-d>", "<C-\\><C-n>", opts)
keymap("t", "<C-w>", "<C-\\><C-n><C-w>", opts)
keymap("t", "<C-h>", "<C-\\><C-n>:bnext<CR>", opts)
keymap("t", "<C-l>", "<C-\\><C-n>:bprev<CR>", opts)
