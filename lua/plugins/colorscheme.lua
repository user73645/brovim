-- set t_Co=256
-- set termguicolors

-- let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
-- let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

-- vim.opt.termguicolors = true
-- vim.opt.background = dark
vim.o.background = "dark" -- or "light" for light mode
vim.cmd([[colorscheme srcery]])
-- vim.cmd([[colorscheme dw_red]])

-- vim.cmd([[colorscheme afterglow]])
vim.cmd('autocmd vimenter * hi Normal guibg=NONE ctermbg=NONE')
-- vim.cmd([[AirlineTheme base16_gruvbox_dark_hard]])

-- let g:deus_termcolors=256
