require "config.options"
require "config.keybinds"

require "plugins.lazy"
require "plugins.bufferline"
require "plugins.colorscheme"
require "plugins.treesitter"
require "plugins.lualine"
require "plugins.mason"
